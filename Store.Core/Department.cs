﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Core
{
    public class Department: Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public int NoOfProducts { get; set; }
    }
}
