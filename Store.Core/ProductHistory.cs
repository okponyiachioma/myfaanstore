﻿using Store.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Core
{
   public class ProductHistory:Entity
    {
        public virtual Product TheProduct { get; set; }
        public virtual string TheDepartment { get; set; }
        public RequestType RequestType { get; set; }
        public DateTime Date { get; set; }
        public int Quantity { get; set; }
        public int QuantityAfter { get; set; }
        public int QuantityBefore { get; set; }
        public string NameOfRepresentative { get; set; }
        public string PhoneOfRepresentative { get; set; }
        public string VoucherNo { get; set; }
        public decimal ValuePrice { get; set; }
        public decimal UnitPrice { get; set; }


    }
}
