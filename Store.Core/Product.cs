﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Core
{
   public class Product: Entity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public virtual Department TheDepartment { get; set; }
    }
}
