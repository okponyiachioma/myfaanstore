﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Core.Exceptions
{
    public class FailureException:ApplicationException
    {
        public FailureException(string message) : base(message)
        {
        }
    }
}
