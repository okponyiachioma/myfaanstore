﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Core
{
    public class ProductAnalysis:Entity
    {
        public int TotalIssued { get; set; }
        public int TotalReceived { get; set; }
        public int TotalDepartment { get; set; }
        public int TotalItems { get; set; }
        public DateTime Date { get; set; }


    }
}
