﻿using Store.Core;
using Store.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Logic.Interfaces
{
    public interface IProductHistoryLogic
    {
        void Save(ProductHistory productHistory);
        void Update(ProductHistory productHistory);
        ProductHistory GetByID(long id);
        IList<ProductHistory> GetAll();
        IList<ProductHistory> Search(DateTime fromDate, DateTime toDate, Product theProduct,Department theDepartment,RequestType requestType,string department);
        int Count(RequestType requestType);
        void SaveChanges();

    }
}
