﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Logic.Interfaces
{
    public interface IProductAnalysisLogic
    {
        void Save(ProductAnalysis ProductAnalysis);
        void Update(ProductAnalysis ProductAnalysis);
        ProductAnalysis GetToday();
        void SaveChanges();

    }
}
