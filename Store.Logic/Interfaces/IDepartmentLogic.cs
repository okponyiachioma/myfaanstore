﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Logic.Interfaces
{
    public interface IDepartmentLogic
    {
        void Save(Department Department);
        void Update(Department Department);
        Department GetByID(long id);
        IList<Department> GetAll();
        IEnumerable<SelectListItem> GetAllViews();
        Department GetByCode(string code);
        void SaveChanges();


    }
}
