﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Logic.Interfaces
{
    public interface IProductLogic
    {
        void Save(Product Product);
        void Update(Product Product);
        Product GetByID(long id);
        IList<Product> GetAll();
        IEnumerable<SelectListItem> GetAllViews();
        Product GetByCode(string code);
        IList<Product> Search(DateTime fromDate, DateTime toDate, Product theProduct, Department theDepartment);
        int Count();
        void SaveChanges();
    }
}
