﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using Store.Data;
using Store.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Logic.Implementation
{
    public class ProductAnalysisLogic : IProductAnalysisLogic
    {
        IProductAnalysisRepository _repository;
       public ProductAnalysisLogic(IProductAnalysisRepository repository)
        {
            this._repository = repository;
        }
        public IList<ProductAnalysis> GetAll()
        {
            return this._repository.GetAll().ToList();
        }
       
        public ProductAnalysis GetToday()
        {
            var today = _repository.GetToday();

            return today;
        }
        public void SaveChanges()
        {
            this._repository.SaveChanges();
        }
        public ProductAnalysis GetByID(long id)
        {
            return this._repository.GetByID(id);
        }

        public void Save(ProductAnalysis ProductAnalysis)
        {
            try
            {
                this._repository.Save(ProductAnalysis);
            }
            catch (Exception ex)
            {
            }
        }

        

        public void Update(ProductAnalysis ProductAnalysis)
        {
            try
            {
                this._repository.Update(ProductAnalysis);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
