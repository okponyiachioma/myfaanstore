﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using Store.Data;
using Store.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Logic.Implementation
{
    public class ProductLogic : IProductLogic
    {
        IProductRepository _repository;
       public ProductLogic(IProductRepository repository)
        {
            this._repository = repository;
        }

        public int Count()
        {
            return _repository.Count();
        }

        public IList<Product> GetAll()
        {
            return this._repository.GetAll().ToList();
        }
        public IEnumerable<SelectListItem> GetAllViews()
        {
            var all = GetAll();
            IEnumerable<SelectListItem> items = all.Select(x =>
                       new SelectListItem
                       {
                           Value = x.ID.ToString(),
                           Text = x.Name+$" ({x.Code})"
                       });
            return items;
        }

        public Product GetByCode(string code)
        {
            return _repository.GetByCode(code);
        }

        public Product GetByID(long id)
        {
            return this._repository.GetByID(id);
        }

        public void Save(Product Product)
        {
            try
            {
                this._repository.Save(Product);
               
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public void SaveChanges()
        {
            this._repository.SaveChanges();
        }

        public IList<Product> Search(DateTime fromDate, DateTime toDate, Product theProduct, Department theDepartment)
        {
            return _repository.Search(fromDate, toDate, theProduct, theDepartment);
        }

        public void Update(Product Product)
        {
            try
            {
                this._repository.Update(Product);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
    }
}
