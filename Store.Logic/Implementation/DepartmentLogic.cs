﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using Store.Data;
using Store.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Logic.Implementation
{
    public class DepartmentLogic : IDepartmentLogic
    {
        IDepartmentRepository _repository;
        public DepartmentLogic(IDepartmentRepository repository)
        {
            this._repository = repository;
        }
        public IList<Department> GetAll()
        {
            return this._repository.GetAll().ToList();
        }
        public IEnumerable<SelectListItem> GetAllViews()
        {
            var all = GetAll();
            IEnumerable<SelectListItem> items = all.Select(x =>
                       new SelectListItem
                       {
                           Value = x.ID.ToString(),
                           Text = x.Name
                       });
            return items;
        }
        public Department GetByCode(string code)
        {
            return _repository.GetByCode(code);
        }
        public Department GetByID(long id)
        {
            return this._repository.GetByID(id);
        }

        public void Save(Department Department)
        {
            try
            {
                this._repository.Save(Department);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Department Department)
        {
            try
            {
                this._repository.Update(Department);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        public void SaveChanges()
        {
            this._repository.SaveChanges();
        }
    }
}
