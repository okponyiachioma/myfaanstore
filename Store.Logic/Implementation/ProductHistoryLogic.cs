﻿using Store.Core;
using Store.Core.Enums;
using Store.Data;
using Store.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Logic.Implementation
{
    public class ProductHistoryLogic : IProductHistoryLogic
    {
        IProductHistoryRepository _repository;
        public ProductHistoryLogic(IProductHistoryRepository repository)
        {
            this._repository = repository;
        }

        public int Count(RequestType requestType)
        {
            return _repository.Count(requestType);
        }

        public IList<ProductHistory> GetAll()
        {
            return this._repository.GetAllEntities()?.ToList();
        }

        public ProductHistory GetByID(long id)
        {
            return this._repository.GetByID(id);
        }

        public void Save(ProductHistory productHistory)
        {
            try
            {
                this._repository.Save(productHistory);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        public void SaveChanges()
        {
            this._repository.SaveChanges();
        }
        public IList<ProductHistory> Search(DateTime fromDate, DateTime toDate, Product theProduct, Department theDepartment, RequestType requestType, string department)
        {
            return _repository.Search(fromDate, toDate, theProduct, theDepartment, requestType, department);
        }

        public void Update(ProductHistory productHistory)
        {
            try
            {
                this._repository.Update(productHistory);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
    }
}
