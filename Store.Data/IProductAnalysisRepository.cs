﻿using Store.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Data
{
    public interface IProductAnalysisRepository:IRepository<ProductAnalysis>
    {
        ProductAnalysis GetToday();

    }
}
