﻿using Store.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Data
{
    public interface IDepartmentRepository : IRepository<Department>
    {
        Department GetByCode(string code);
    }
}
