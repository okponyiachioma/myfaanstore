﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Data
{
    public interface IRepository<T>
    {
        void Save(T entity);
        void Save(IList<T> entity);
        void Update(T model);
        T GetByID(object id);
        IQueryable<T> GetAll();
        void Delete(object id);
        void Delete(T entityToDelete);
        void SaveChanges();


    }
}
