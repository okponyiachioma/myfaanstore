﻿using Store.Core;
using Store.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Data
{
    public interface IProductHistoryRepository : IRepository<ProductHistory>
    {
        IQueryable<ProductHistory> GetAllEntities();
        IList<ProductHistory> Search(DateTime fromDate, DateTime toDate, Product theProduct, Department theDepartment, RequestType requestType, string department);

        int Count(RequestType requestType);
    }
}
