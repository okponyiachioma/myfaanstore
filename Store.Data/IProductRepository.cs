﻿using Store.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Data
{
    public interface IProductRepository:IRepository<Product>
    {
        Product GetByCode(string code);
        IList<Product> Search(DateTime fromDate, DateTime toDate, Product theProduct, Department theDepartment);
        int Count();
    }
}
