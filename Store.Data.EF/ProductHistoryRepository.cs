﻿using Microsoft.EntityFrameworkCore;
using Store.Core;
using Store.Core.Enums;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;

namespace Store.Data.EF
{
    public class ProductHistoryRepository : Repository<ProductHistory>, IProductHistoryRepository
    {
        public ProductHistoryRepository(StoreDBContext context) : base(context)
        {
            this.context = context;

        }

        public int Count(RequestType requestType)
        {
            return this.dbSet.Where(x => x.RequestType == requestType).Count();
        }

        public IQueryable<ProductHistory> GetAllEntities()
        {
            return this.dbSet
                  .Include(history => history.TheProduct);

        }

        public override ProductHistory GetByID(object id)
        {
            var history = this.dbSet.Find(id);
            if (history == null) return null;

            this.context.Entry(history).Reference(h => h.TheProduct).Load();
            return history;
        }

        public IList<ProductHistory> Search(DateTime fromDate, DateTime toDate, Product theProduct, Department theDepartment, RequestType requestType, string department)
        {
            IQueryable<ProductHistory> criteria = this.dbSet.Include(x=>x.TheProduct);

            if (theProduct != null)
                criteria = criteria.Where(x => x.TheProduct.ID == theProduct.ID);
            if (theDepartment != null)
                criteria = criteria.Where(x => x.TheProduct.TheDepartment.ID == theDepartment.ID);
            if (requestType != 0)
                criteria = criteria.Where(x => x.RequestType == requestType);
            if (!string.IsNullOrEmpty(department))
                criteria = criteria.Where(x => x.TheDepartment.Contains(department));
            if (fromDate != new DateTime() && fromDate > SqlDateTime.MinValue)
                criteria = criteria.Where(x => x.Date >= fromDate);
            if (toDate != new DateTime() && toDate > SqlDateTime.MinValue)
            {
                criteria = criteria.Where(x => x.Date.AddDays(1) <= toDate);
            }
            
            return criteria.ToList();



        }
    }
}
