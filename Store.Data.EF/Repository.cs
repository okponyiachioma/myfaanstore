﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Data.EF
{
    public class Repository<T> : IRepository<T> where T : class
    {
        internal StoreDBContext context;
        internal DbSet<T> dbSet;

        public Repository(StoreDBContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<T>();

        }
        public virtual T GetByID(object id)
        {
            return dbSet.Find(id);
        }
        public virtual IQueryable<T> GetAll()
        {
            return dbSet;
        }

        public virtual void Save(T entity)
        {
            dbSet.Add(entity);
            //this.context.SaveChanges();

        }
        public virtual void Save(IList<T> entity)
        {
             dbSet.AddRange(entity);
            //this.context.SaveChanges();
        }


        public virtual void Delete(object id)
        {
            T entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(T entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
            //this.context.SaveChanges();
        }

        public virtual void Update(T entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
            //this.context.SaveChanges();
        }

        public void SaveChanges()
        {
            this.context.SaveChanges();
        }
    }
}
