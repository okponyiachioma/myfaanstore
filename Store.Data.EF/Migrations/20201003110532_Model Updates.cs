﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Data.EF.Migrations
{
    public partial class ModelUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhoneOfRepresentative",
                table: "ProductHistories",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QuantityBefore",
                table: "ProductHistories",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "Departments",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhoneOfRepresentative",
                table: "ProductHistories");

            migrationBuilder.DropColumn(
                name: "QuantityBefore",
                table: "ProductHistories");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "Departments");
        }
    }
}
