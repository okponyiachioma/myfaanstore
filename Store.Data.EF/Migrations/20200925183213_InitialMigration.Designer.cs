﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Store.Data.EF;

namespace Store.Data.EF.Migrations
{
    [DbContext(typeof(StoreDBContext))]
    [Migration("20200925183213_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.11-servicing-32099")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Store.Core.Department", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Departments");
                });

            modelBuilder.Entity("Store.Core.Product", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code");

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int>("Quantity");

                    b.Property<decimal>("UnitPrice");

                    b.HasKey("ID");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("Store.Core.ProductHistory", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Date");

                    b.Property<string>("NameOfRepresentative");

                    b.Property<int>("Quantity");

                    b.Property<int>("QuantityAfter");

                    b.Property<int>("RequestType");

                    b.Property<long?>("TheDepartmentID");

                    b.Property<long?>("TheProductID");

                    b.Property<decimal>("ValuePrice");

                    b.Property<string>("VoucherNo");

                    b.HasKey("ID");

                    b.HasIndex("TheDepartmentID");

                    b.HasIndex("TheProductID");

                    b.ToTable("ProductHistories");
                });

            modelBuilder.Entity("Store.Core.ProductHistory", b =>
                {
                    b.HasOne("Store.Core.Department", "TheDepartment")
                        .WithMany()
                        .HasForeignKey("TheDepartmentID");

                    b.HasOne("Store.Core.Product", "TheProduct")
                        .WithMany()
                        .HasForeignKey("TheProductID");
                });
#pragma warning restore 612, 618
        }
    }
}
