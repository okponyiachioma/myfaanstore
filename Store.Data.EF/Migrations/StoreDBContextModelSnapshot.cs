﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Store.Data.EF;

namespace Store.Data.EF.Migrations
{
    [DbContext(typeof(StoreDBContext))]
    partial class StoreDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.11-servicing-32099")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Store.Core.Department", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code");

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int>("NoOfProducts");

                    b.HasKey("ID");

                    b.ToTable("Departments");
                });

            modelBuilder.Entity("Store.Core.Product", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Code");

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int>("Quantity");

                    b.Property<long?>("TheDepartmentID");

                    b.Property<decimal>("UnitPrice");

                    b.HasKey("ID");

                    b.HasIndex("TheDepartmentID");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("Store.Core.ProductAnalysis", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Date");

                    b.Property<int>("TotalDepartment");

                    b.Property<int>("TotalIssued");

                    b.Property<int>("TotalItems");

                    b.Property<int>("TotalReceived");

                    b.HasKey("ID");

                    b.ToTable("ProductAnalysis");
                });

            modelBuilder.Entity("Store.Core.ProductHistory", b =>
                {
                    b.Property<long>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Date");

                    b.Property<string>("NameOfRepresentative");

                    b.Property<string>("PhoneOfRepresentative");

                    b.Property<int>("Quantity");

                    b.Property<int>("QuantityAfter");

                    b.Property<int>("QuantityBefore");

                    b.Property<int>("RequestType");

                    b.Property<string>("TheDepartment");

                    b.Property<long?>("TheProductID");

                    b.Property<decimal>("UnitPrice");

                    b.Property<decimal>("ValuePrice");

                    b.Property<string>("VoucherNo");

                    b.HasKey("ID");

                    b.HasIndex("TheProductID");

                    b.ToTable("ProductHistories");
                });

            modelBuilder.Entity("Store.Core.Product", b =>
                {
                    b.HasOne("Store.Core.Department", "TheDepartment")
                        .WithMany()
                        .HasForeignKey("TheDepartmentID");
                });

            modelBuilder.Entity("Store.Core.ProductHistory", b =>
                {
                    b.HasOne("Store.Core.Product", "TheProduct")
                        .WithMany()
                        .HasForeignKey("TheProductID");
                });
#pragma warning restore 612, 618
        }
    }
}
