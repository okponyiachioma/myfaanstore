﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Data.EF.Migrations
{
    public partial class UpdatedModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductHistories_Departments_TheDepartmentID",
                table: "ProductHistories");

            migrationBuilder.DropIndex(
                name: "IX_ProductHistories_TheDepartmentID",
                table: "ProductHistories");

            migrationBuilder.DropColumn(
                name: "TheDepartmentID",
                table: "ProductHistories");

            migrationBuilder.AddColumn<long>(
                name: "TheDepartmentID",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TheDepartment",
                table: "ProductHistories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_TheDepartmentID",
                table: "Products",
                column: "TheDepartmentID");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Departments_TheDepartmentID",
                table: "Products",
                column: "TheDepartmentID",
                principalTable: "Departments",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Departments_TheDepartmentID",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_TheDepartmentID",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TheDepartmentID",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TheDepartment",
                table: "ProductHistories");

            migrationBuilder.AddColumn<long>(
                name: "TheDepartmentID",
                table: "ProductHistories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductHistories_TheDepartmentID",
                table: "ProductHistories",
                column: "TheDepartmentID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductHistories_Departments_TheDepartmentID",
                table: "ProductHistories",
                column: "TheDepartmentID",
                principalTable: "Departments",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
