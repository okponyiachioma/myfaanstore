﻿using Store.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Store.Data.EF
{
    public class DepartmentRepository : Repository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(StoreDBContext context) : base(context)
        {
            this.context = context;
            
        }

        public Department GetByCode(string code)
        {
            return this.dbSet.Where(x => x.Code == code).FirstOrDefault();
        }
    }
}
