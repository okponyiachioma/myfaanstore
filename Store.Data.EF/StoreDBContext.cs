﻿using Microsoft.EntityFrameworkCore;
using Store.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Store.Data.EF
{
   public  class StoreDBContext: DbContext
    {
        public StoreDBContext(DbContextOptions options) : base(options)
        {
        }

        DbSet<Product> Products { get; set; }
        DbSet<Department> Departments { get; set; }
        DbSet<ProductHistory> ProductHistories { get; set; }
        DbSet<ProductAnalysis> ProductAnalysis { get; set; }

    }
}
