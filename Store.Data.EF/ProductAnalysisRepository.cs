﻿using Microsoft.EntityFrameworkCore;
using Store.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;

namespace Store.Data.EF
{
    public class ProductAnalysisRepository : Repository<ProductAnalysis>, IProductAnalysisRepository
    {
        public ProductAnalysisRepository(StoreDBContext context) : base(context)
        {
            this.context = context;
            
        }

      
        public ProductAnalysis GetToday()
        {
            return this.dbSet.Where(x => x.Date.Date == DateTime.Now.Date).FirstOrDefault();
        }

      
    }
}
