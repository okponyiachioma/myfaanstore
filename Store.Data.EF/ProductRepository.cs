﻿using Microsoft.EntityFrameworkCore;
using Store.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;

namespace Store.Data.EF
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(StoreDBContext context) : base(context)
        {
            this.context = context;

        }

        public int Count()
        {
            return this.dbSet.Count();
        }

        public Product GetByCode(string code)
        {
            return this.dbSet.Include(x => x.TheDepartment).Where(x => x.Code == code).FirstOrDefault();
        }
        public override Product GetByID(object id)
        {


            var product = this.dbSet.Find(id);
            if (product == null) return null;
            this.context.Entry(product).Reference(h => h.TheDepartment).Load();
            return product;

        }
        public IQueryable<Product> GetAll()
        {
            return this.dbSet
                .Include(p => p.TheDepartment);

        }

        public IList<Product> Search(DateTime fromDate, DateTime toDate, Product theProduct, Department theDepartment)
        {

            IQueryable<Product> criteria = this.dbSet.Include(x => x.TheDepartment);

            if (theProduct != null)
                criteria = criteria.Where(x => x.ID == theProduct.ID);
            if (theDepartment != null)
                criteria = criteria.Where(x => x.TheDepartment.ID == theDepartment.ID);

            if (fromDate != new DateTime() && fromDate > SqlDateTime.MinValue)
                criteria = criteria.Where(x => x.DateCreated >= fromDate);
            if (toDate != new DateTime() && toDate > SqlDateTime.MinValue)
            {
                criteria = criteria.Where(x => x.DateCreated.AddDays(1) <= toDate);
            }

            return criteria.ToList();

        }
    }
}
