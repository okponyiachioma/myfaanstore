﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyFAANStore.Models;
using Store.Core;
using Store.Core.Exceptions;
using Store.Logic.Interfaces;

namespace MyFAANStore.Controllers
{
    public class ProductController : Controller
    {
        IProductLogic ProductLogic;
        IDepartmentLogic DepartmentLogic;
        IProductAnalysisLogic ProductAnalysisLogic;
        ProductAnalysis InventoryAnalysis;
        public ProductController(IProductLogic productLogic, IDepartmentLogic departmentLogic, IProductAnalysisLogic productAnalysisLogic)
        {
            this.ProductLogic = productLogic;
            this.DepartmentLogic = departmentLogic;
            this.ProductAnalysisLogic = productAnalysisLogic;
            InventoryAnalysis = this.ProductAnalysisLogic.GetToday();
        }
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            var product = ProductLogic.GetByID(id);
            if (product == null)
            {
                TempData["Error"] = "Invalid ID";
                return RedirectToAction("ViewProducts");
            }
            var model = new ProductDetailsModel
            {
                Name = product.Name,
                Code = product.Code,
                DateCreated = product.DateCreated,
                Department = product.TheDepartment?.Name,
                Description = product.Description,
                Quantity = product.Quantity,
                UnitPrice = product.UnitPrice,
                ID = product.ID
            };



            return PartialView("Details", model);
        }
        // GET: Product/Details/5
        public ActionResult Alter(int id)
        {
            var product = ProductLogic.GetByID(id);
            if (product == null)
            {
                TempData["Error"] = "Invalid ID";
                return RedirectToAction("ViewProducts");
            }
            var model = new ProductDetailsModel
            {
                Name = product.Name,
                Code = product.Code,
                DateCreated = product.DateCreated,
                Department = product.TheDepartment?.Name,
                Description = product.Description,
                Quantity = product.Quantity,
                UnitPrice = product.UnitPrice,
                ID = product.ID
            };



            return View("Alter", new ProductCreateModel());
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            ProductCreateModel model = new ProductCreateModel { };
            model.Departments = this.DepartmentLogic.GetAllViews();
            return View(model);
        }

        // POST: Product/Create
        [HttpPost]

        public ActionResult Create(ProductCreateModel model)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (this.ProductLogic.GetByCode(model.Code) != null)
                    {
                        throw new FailureException("Code already exists. Please use another code");
                    }
                    var dep = this.DepartmentLogic.GetByID(model.DepartmentID.Value);
                    dep.NoOfProducts++;
                    this.DepartmentLogic.Update(dep);
                    ProductLogic.Save(new Store.Core.Product
                    {
                        Code = model.Code,
                        Description = model.Description,
                        Name = model.Name,
                        Quantity = model.Quantity.Value,
                        UnitPrice = model.UnitPrice.Value,
                        DateCreated = DateTime.Now,
                        TheDepartment = dep

                    });

                    InventoryAnalysis.TotalItems++;
                    ProductAnalysisLogic.Update(InventoryAnalysis);
                    ProductLogic.SaveChanges();
                    ViewBag.Success = $"{model.Name} Successfully Saved";
                    ModelState.Clear();

                }

            }

            catch (FailureException ex)
            {
                ViewBag.Error = ex.Message;

            }
            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";

            }
            model.Departments = this.DepartmentLogic.GetAllViews();
            return View(model);
        }
        public ActionResult ViewProducts(ProductViewModel model)
        {
            try
            {
                var product = ProductLogic.GetAll();

                if (product != null)
                    product = product.OrderByDescending(x => x.DateCreated).ToList();
                ProductViewModel theModel = new ProductViewModel { TheProducts = product };

                return View(theModel);
            }
            catch
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";
                return View();
            }
        }
        // GET: Product/Edit/5
        public ActionResult Edit(long id)
        {
            if (id == 0) return View();
            var product = ProductLogic.GetByID(id);
            if (product == null)
            {
                TempData["Error"] = "Invalid ID";
                return RedirectToAction("ViewProductHistorys");
            }
            var model = new ProductCreateModel
            {
                Departments = this.DepartmentLogic.GetAllViews(),
                Code = product.Code,
                Description = product.Description,
                Name = product.Name,
                Quantity = product.Quantity,
                UnitPrice = product.UnitPrice,
                DepartmentID = product.TheDepartment?.ID,
                ID = product.ID
            };

            return View("Edit", model);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(ProductCreateModel model)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    var existing = this.ProductLogic.GetByCode(model.Code);
                    var previousDep = existing.TheDepartment;
                    if (existing != null && existing.ID != model.ID)
                    {
                        throw new FailureException("Code already exists. Please use another code");
                    }

                    
                    var dep = this.DepartmentLogic.GetByID(model.DepartmentID.Value);
                    existing.Name = model.Name;
                    existing.Code = model.Code;
                    existing.Description = model.Description;
                    existing.TheDepartment = dep;
                    existing.UnitPrice = model.UnitPrice.Value;
                    existing.Quantity = model.Quantity.Value;

                    if (previousDep.ID != dep.ID)
                    {
                        previousDep.NoOfProducts--;
                        dep.NoOfProducts++;
                        this.DepartmentLogic.Update(dep);
                        this.DepartmentLogic.Update(previousDep);

                    }
                    else
                    {
                        dep.NoOfProducts++;
                        this.DepartmentLogic.Update(dep);
                    }


                    this.ProductLogic.Update(existing);
                    ProductLogic.SaveChanges();
                    ViewBag.Success = $"{model.Name} Successfully Updated";
                    ModelState.Clear();

                }

            }

            catch (FailureException ex)
            {
                ViewBag.Error = ex.Message;

            }
            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";

            }
            var product = ProductLogic.GetAll();

            ProductViewModel theModel = new ProductViewModel { TheProducts = product };
            return View("ViewProducts", theModel);
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }


        // GET: ProductHistory/Export
        public ActionResult Export()
        {
            ProductExportModel model = new ProductExportModel
            {
                Departments = DepartmentLogic.GetAllViews(),
            };

            return PartialView("Export", model);
        }

        // POST: ProductHistory/Delete/
        [HttpPost]
        public ActionResult Export(ProductHistoryExportModel model)
        {
            try
            {
                // TODO: Add delete logic here
                bool categorizebyDepartment = true;
                Product theProduct = null;
                Department theDepartment = null;

                if (!string.IsNullOrEmpty(model.ProductCode))
                {

                    theProduct = ProductLogic.GetByCode(model.ProductCode);
                    if (theProduct == null)
                    {
                        ViewBag.Error = "Invalid Product Code";
                        return RedirectToAction("ViewProducts");
                    }
                }

                if (model.DepartmentID.HasValue && model.DepartmentID.Value > 0)
                {
                    theDepartment = DepartmentLogic.GetByID(model.DepartmentID.Value);
                }
                DateTime from;
                DateTime to;

                DateTime.TryParse(model.FromDate, out from);
                DateTime.TryParse(model.ToDate, out to);


                var result = ProductLogic.Search(from, to, theProduct, theDepartment);

                if (result == null || result.Count == 0)
                {
                    ViewBag.Error = "No Data Found";
                    return RedirectToAction("ViewProducts");
                }

                var grouping = result.GroupBy(x => x.TheDepartment).Select(y => new ProductGroup()
                {
                    TheDepartment = y.Key,
                    Product = y.ToList()
                });
                using (var workbook = new XLWorkbook())
                {
                    foreach (var groupItem in grouping)
                    {
                        var worksheet = workbook.Worksheets.Add(string.IsNullOrEmpty(groupItem.TheDepartment.Name) ? "Product" : groupItem.TheDepartment.Name + groupItem.TheDepartment.ID);
                        IXLRange titlerange = worksheet.Range(worksheet.Cell(1, 2).Address, worksheet.Cell(1, 8).Address);
                        titlerange.Merge().Style.Font.SetBold().Font.FontSize = 13;
                        titlerange.Value = "FEDERAL AIRPORTS AUTHORITY OF NIGERIA";

                        var issued = worksheet.Range(worksheet.Cell(2, 2).Address, worksheet.Cell(2, 8).Address);
                        issued.Merge().Style.Font.SetBold().Font.FontSize = 13;
                        issued.Value = "STORES DEPARTMENT, HEADQUARTERS LAGOS.";

                        var balanced = worksheet.Range(worksheet.Cell(3, 1).Address, worksheet.Cell(3, 6).Address);
                        balanced.Merge().Style.Font.SetBold().Font.FontSize = 13;
                        balanced.Value = $"{groupItem.TheDepartment.Name} Department";

                        var header = worksheet.Range(worksheet.Cell(4, 1).Address, worksheet.Cell(4, 11).Address);
                        header.Style.Font.SetBold().Font.FontSize = 10;

                        var currentRow = 4;
                        worksheet.Cell(currentRow, 1).Value = "Code";
                        worksheet.Cell(currentRow, 2).Value = "Description";
                        worksheet.Cell(currentRow, 3).Value = "Bal B/F";
                        worksheet.Cell(currentRow, 4).Value = "Unit Price";
                        worksheet.Cell(currentRow, 5).Value = "Value Of Balance";




                        foreach (var item in groupItem.Product)
                        {
                            currentRow++;
                            worksheet.Cell(currentRow, 1).Value = item.Code;
                            worksheet.Cell(currentRow, 2).Value = item.Name;
                            worksheet.Cell(currentRow, 3).Value = item.Quantity;
                            worksheet.Cell(currentRow, 4).Value = item.UnitPrice;
                            worksheet.Cell(currentRow, 5).Value = item.UnitPrice * item.Quantity;

                        }
                    }



                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(
                            content,
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            "Store Item Inventory.xlsx");
                    }

                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("ViewProducts");
            }
        }
    }
    public class ProductGroup
    {
        public Department TheDepartment { get; set; }
        public List<Product> Product { get; set; }

    }
}