﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyFAANStore.Models;
using Store.Core;
using Store.Core.Exceptions;
using Store.Logic.Interfaces;

namespace MyFAANStore.Controllers
{
    public class ProductHistoryController : Controller
    {
        IProductHistoryLogic ProductHistoryLogic;
        IDepartmentLogic DepartmentLogic;
        IProductLogic ProductLogic;
        IProductAnalysisLogic ProductAnalysisLogic;
        ProductAnalysis InventoryAnalysis;
        public ProductHistoryController(IProductHistoryLogic ProductHistoryLogic, IDepartmentLogic departmentLogic, IProductLogic productLogic, IProductAnalysisLogic productAnalysisLogic)
        {
            this.ProductHistoryLogic = ProductHistoryLogic;
            this.ProductLogic = productLogic;
            this.DepartmentLogic = departmentLogic;
            this.ProductAnalysisLogic = productAnalysisLogic;
            InventoryAnalysis = this.ProductAnalysisLogic.GetToday();
        }
        // GET: ProductHistory
        public ActionResult Index()
        {
            return View();
        }


        // GET: ProductHistory
        public ActionResult Suply()
        {
            ProductHistoryModel model = new ProductHistoryModel { };
            model.Departments = this.DepartmentLogic.GetAllViews();
            model.Products = this.ProductLogic.GetAllViews()?.OrderBy(x => x.Text);
            return View(model);
        }
        [HttpPost]
        public ActionResult Suply(ProductHistoryModel model)
        {
            try
            {
                //TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    var theProduct = this.ProductLogic.GetByID(model.ProductID.Value);

                    var productHistory = new ProductHistory
                    {
                        TheProduct = theProduct,
                        TheDepartment = model.Department,
                        Quantity = model.Quantity.Value,
                        QuantityAfter = theProduct.Quantity + model.Quantity.Value,
                        QuantityBefore = theProduct.Quantity,
                        Date = DateTime.Now,
                        RequestType = Store.Core.Enums.RequestType.Receive,
                        VoucherNo = theProduct.Code,
                        NameOfRepresentative = model.NameOfRepresentative,
                        PhoneOfRepresentative = model.PhoneOfRepresentative,
                        UnitPrice =  theProduct.UnitPrice ,

                    };
                    productHistory.ValuePrice = productHistory.UnitPrice * productHistory.Quantity;
                    theProduct.Quantity = theProduct.Quantity + model.Quantity.Value;
                    this.InventoryAnalysis.TotalReceived+=model.Quantity.Value;
                    this.ProductAnalysisLogic.Update(this.InventoryAnalysis);
                    this.ProductHistoryLogic.Save(productHistory);
                    this.ProductLogic.Update(theProduct);
                    this.ProductLogic.SaveChanges();


                    ViewBag.Success = $"{theProduct.Name} Successfully Received";
                    ModelState.Clear();
                    return View("Verify", new ProductVerifyModel { IsSupply = true });

                }

               
            }


            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";
            }
           
            return View(model);
        }

        // GET: ProductHistory
        public ActionResult Verify( bool IsSupply)
        {
            ProductVerifyModel model = new ProductVerifyModel {  IsSupply = IsSupply };
            return View(model);
        }
        [HttpPost]
        public ActionResult Verify(ProductVerifyModel model)
        {
            try
            {

                //TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    var theProduct = this.ProductLogic.GetByCode(model.ProductCode);

                    if (theProduct == null) throw new FailureException("Invalid Code. No product exists with this Code. Please check the list of available products and try again");


                    ProductHistoryModel history = new ProductHistoryModel { ProductCode = model.ProductCode, ProductName = theProduct.Name, QuantityRemaining = theProduct.Quantity, ProductID = theProduct.ID };

                    if (!model.IsSupply)
                        return View("MakeRequest", history);
                    else
                        return View("Suply", history);

                }
            }
            catch (FailureException ex)
            {
                ViewBag.Error = ex.Message;
            }

            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";

            }
            return View();
        }
        public ActionResult MakeRequest()
        {
            ProductHistoryModel model = new ProductHistoryModel { };
            model.Departments = this.DepartmentLogic.GetAllViews();
            model.Products = this.ProductLogic.GetAllViews()?.OrderBy(x => x.Text);
            return View(model);
        }
        // GET: ProductHistory
        [HttpPost]
        public ActionResult MakeRequest(ProductHistoryModel model)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    var theProduct = this.ProductLogic.GetByID(model.ProductID.Value);


                    if (model.Quantity > theProduct.Quantity) throw new FailureException("The quantity requested is more than what is available");
                    var productHistory = new ProductHistory
                    {
                        TheProduct = theProduct,
                        TheDepartment = model.Department,
                        Quantity = model.Quantity.Value,
                        QuantityAfter = theProduct.Quantity - model.Quantity.Value,
                        QuantityBefore = theProduct.Quantity,
                        Date = DateTime.Now,
                        RequestType = Store.Core.Enums.RequestType.Issue,
                        VoucherNo = theProduct.Code,
                        NameOfRepresentative = model.NameOfRepresentative,
                        PhoneOfRepresentative = model.PhoneOfRepresentative,
                        UnitPrice=theProduct.UnitPrice
                    };
                    productHistory.ValuePrice = productHistory.UnitPrice * productHistory.Quantity;
                    theProduct.Quantity = theProduct.Quantity - model.Quantity.Value;
                    this.InventoryAnalysis.TotalIssued += model.Quantity.Value;
                    this.ProductAnalysisLogic.Update(this.InventoryAnalysis);
                    this.ProductHistoryLogic.Save(productHistory);
                    this.ProductLogic.Update(theProduct);
                    this.ProductLogic.SaveChanges();


                    ViewBag.Success = $"{theProduct.Name} Successfully Issued";
                    ModelState.Clear();
                    return View("Verify", new ProductVerifyModel { IsSupply = false });

                }


            }

            catch (FailureException ex)
            {
                ViewBag.Error = ex.Message;
            }

            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";
            }

          
            return View(model);
        }
        // GET: ProductHistory/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ProductHistory/Create
        public ActionResult Create()
        {
            return View();
        }


        public ActionResult ViewProductHistorys(ProductHistoryViewModel model)
        {
            try
            {
                var productHistory = ProductHistoryLogic.GetAll();

                productHistory = productHistory.OrderByDescending(x => x.Date).ToList();
                var theModel = new ProductHistoryViewModel { TheProductHistories = productHistory };

                return View(theModel);
            }
            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";
                return View();
            }
        }
        // GET: ProductHistory/Edit/5
        public ActionResult Edit(int id)
        {
            var productHistory = ProductHistoryLogic.GetByID(id);
            if (productHistory == null)
            {
                TempData["Error"] = "Invalid ID";
                return RedirectToAction("ViewProductHistorys");
            }
            ProductHistoryDetailsViewModel model = new ProductHistoryDetailsViewModel
            {
                ProductCode = productHistory.TheProduct?.Code,
                ProductName = productHistory.TheProduct?.Name,
                Date = productHistory.Date,
                Department = productHistory.TheDepartment,
                NameOfRepresentative = productHistory.NameOfRepresentative,
                PhoneOfRepresentative = productHistory.NameOfRepresentative,
                Quantity = productHistory.Quantity,
                QuantityAfter = productHistory.QuantityAfter,
                QuantityBefore = productHistory.QuantityBefore,
                RequestType = productHistory.RequestType,
                UnitPrice = productHistory.UnitPrice,
                ValuePrice = productHistory.ValuePrice
            };



            return PartialView("Details", model);
        }
        // GET: ProductHistory/Edit/5
        public ActionResult EditEntry(int id)
        {
            var productHistory = ProductHistoryLogic.GetByID(id);
            if (productHistory == null)
            {
                TempData["Error"] = "Invalid ID";
                return RedirectToAction("ViewProductHistorys");
            }
          
            ProductHistoryModel model = new ProductHistoryModel
            {
                ProductName = productHistory.TheProduct?.Name,
                Quantity  = productHistory.Quantity,
                ProductCode = productHistory.TheProduct?.Code,
                Department=productHistory.TheDepartment,
                QuantityRemaining= productHistory.TheProduct.Quantity,
                NameOfRepresentative=productHistory.NameOfRepresentative,
                PhoneOfRepresentative=productHistory.PhoneOfRepresentative,
                ID=productHistory.ID,
                ProductID=productHistory.TheProduct.ID


            };
           

            return View(model);
        }

        // POST: ProductHistory/Edit/5
        [HttpPost]
        public ActionResult EditEntry(ProductHistoryModel model)
        {
            try
            {
                //TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    var history = this.ProductHistoryLogic.GetByID(model.ID);

                    if (history == null) throw new FailureException("Invalid Data");

                    var theProduct = this.ProductLogic.GetByID(model.ProductID.Value);

                    int difference = history.Quantity - model.Quantity.Value;
                    switch (history.RequestType)
                    {
                        case Store.Core.Enums.RequestType.Receive:
                            difference = -1 * difference;
                            history.QuantityAfter += difference;
                            theProduct.Quantity += difference;
                            this.InventoryAnalysis.TotalReceived += difference;
                            break;
                        case Store.Core.Enums.RequestType.Issue:
                            theProduct.Quantity += difference;
                            history.QuantityAfter += difference;
                            this.InventoryAnalysis.TotalIssued += difference;

                            break;
                        default:
                            break;
                    }
                    history.NameOfRepresentative = model.NameOfRepresentative;
                    history.PhoneOfRepresentative = model.PhoneOfRepresentative;
                    history.TheDepartment = model.Department;
                    history.Quantity = model.Quantity.Value;                                   
                    history.ValuePrice = theProduct.UnitPrice * history.Quantity;

                    this.ProductAnalysisLogic.Update(this.InventoryAnalysis);
                    this.ProductHistoryLogic.Update(history);
                    this.ProductLogic.Update(theProduct);
                    this.ProductLogic.SaveChanges();


                    ViewBag.Success = $"Successfully Updated";
                    ModelState.Clear();
                    return RedirectToAction("ViewProductHistorys");

                }


            }
 catch (FailureException ex)
            {
                ViewBag.Error = ex.Message;

            }

            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";
            }

            return View(model);
        }

        // GET: ProductHistory/Export
        public ActionResult Export()
        {
            ProductHistoryExportModel model = new ProductHistoryExportModel
            {
                Departments = DepartmentLogic.GetAllViews(),
            };

            return PartialView("Export", model);
        }

        // POST: ProductHistory/Delete/
        [HttpPost]
        public ActionResult Export(ProductHistoryExportModel model)
        {
            try
            {
                // TODO: Add delete logic here
                bool categorizebyDepartment = true;
                Product theProduct = null;
                Department theDepartment = null;

                if (!string.IsNullOrEmpty(model.ProductCode))
                {

                    theProduct = ProductLogic.GetByCode(model.ProductCode);
                    if (theProduct == null)
                    {
                        ViewBag.Error = "Invalid Product Code";
                        return RedirectToAction("ViewProductHistorys");
                    }
                }

                if (model.DepartmentID.HasValue && model.DepartmentID.Value > 0)
                {
                    theDepartment = DepartmentLogic.GetByID(model.DepartmentID.Value);
                }
                DateTime from;
                DateTime to;

                DateTime.TryParse(model.FromDate, out from);
                DateTime.TryParse(model.ToDate, out to);


                var result = ProductHistoryLogic.Search(from, to, theProduct, theDepartment, model.RequestType, "");

                if (result == null || result.Count == 0)
                {
                    ViewBag.Error = "No Data Found";
                    return RedirectToAction("ViewProductHistorys");
                }
                
                var grouping = result.GroupBy(x => x.TheProduct).Select(y => new HistoryGroup()
                {
                    TheProduct = y.Key,
                    ProductHistory = y.ToList()
                });
                using (var workbook = new XLWorkbook())
                {
                    foreach (var groupItem in grouping)
                    {
                        var worksheet = workbook.Worksheets.Add(string.IsNullOrEmpty(groupItem.TheProduct.Name)?"Product": groupItem.TheProduct.Name + groupItem.TheProduct.ID);
                        IXLRange titlerange = worksheet.Range(worksheet.Cell(1, 4).Address, worksheet.Cell(1, 6).Address);
                        titlerange.Merge().Style.Font.SetBold().Font.FontSize = 13;
                        titlerange.Value = "Receipts";

                        var issued = worksheet.Range(worksheet.Cell(1, 7).Address, worksheet.Cell(1, 9).Address);
                        issued.Merge().Style.Font.SetBold().Font.FontSize = 13;
                        issued.Value = "Issued";

                        var balanced = worksheet.Range(worksheet.Cell(1, 10).Address, worksheet.Cell(1, 11).Address);
                        balanced.Merge().Style.Font.SetBold().Font.FontSize = 13;
                        balanced.Value = "Balances";

                        var header = worksheet.Range(worksheet.Cell(2, 1).Address, worksheet.Cell(2, 11).Address);
                        header.Style.Font.SetBold().Font.FontSize = 10;

                        var currentRow = 2;
                        worksheet.Cell(currentRow, 1).Value = "Date";
                        worksheet.Cell(currentRow, 2).Value = "From whom received or To whom issued";
                        worksheet.Cell(currentRow, 3).Value = "Voucher No.";
                        worksheet.Cell(currentRow, 4).Value = "Received Quantity";
                        worksheet.Cell(currentRow, 5).Value = "Received Unit Price";
                        worksheet.Cell(currentRow, 6).Value = "Received Value";
                        worksheet.Cell(currentRow, 7).Value = "Issued Quantity";
                        worksheet.Cell(currentRow, 8).Value = "Issued Unit Price";
                        worksheet.Cell(currentRow, 9).Value = "Issued Value";
                        worksheet.Cell(currentRow, 10).Value = "Quantity Before";
                        worksheet.Cell(currentRow, 11).Value = "Quantity After";



                        foreach (var item in groupItem.ProductHistory)
                        {
                            currentRow++;
                            worksheet.Cell(currentRow, 1).Value = item.Date;
                            worksheet.Cell(currentRow, 2).Value = item.TheDepartment;
                            worksheet.Cell(currentRow, 3).Value = item.TheProduct.Code;

                            switch (item.RequestType)
                            {
                                case Store.Core.Enums.RequestType.Receive:
                                    worksheet.Cell(currentRow, 4).Value = item.Quantity;
                                    worksheet.Cell(currentRow, 5).Value = item.UnitPrice;
                                    worksheet.Cell(currentRow, 6).Value = item.ValuePrice;

                                    break;
                                case Store.Core.Enums.RequestType.Issue:
                                    worksheet.Cell(currentRow, 7).Value = item.Quantity;
                                    worksheet.Cell(currentRow, 8).Value = item.UnitPrice;
                                    worksheet.Cell(currentRow, 9).Value = item.ValuePrice;
                                    break;
                                default:
                                    break;
                            }
                            worksheet.Cell(currentRow, 10).Value = item.QuantityBefore;
                            worksheet.Cell(currentRow, 11).Value = item.QuantityAfter;

                        }
                    }

                    

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();

                            return File(
                                content,
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                "Store Inventory.xlsx");
                        }
                    
                }

            }
            catch(Exception ex)
            {
                return RedirectToAction("ViewProductHistorys");
            }
        }
    }
    public class HistoryGroup
    {
        public Product TheProduct { get; set; }
        public List<ProductHistory> ProductHistory { get; set; }

    }
}