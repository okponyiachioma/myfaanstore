﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyFAANStore.Models;
using Store.Core;
using Store.Core.Exceptions;
using Store.Logic.Interfaces;

namespace MyFAANStore.Controllers
{
    public class DepartmentController : Controller
    {
        IDepartmentLogic DepartmentLogic;
        IProductAnalysisLogic ProductAnalysisLogic;
        ProductAnalysis InventoryAnalysis;
        public DepartmentController(IDepartmentLogic DepartmentLogic, IProductAnalysisLogic productAnalysisLogic)
        {
            this.DepartmentLogic = DepartmentLogic;
            this.ProductAnalysisLogic = productAnalysisLogic;
            InventoryAnalysis = this.ProductAnalysisLogic.GetToday();
        }
        // GET: Department
        public ActionResult Index()
        {
            return View();
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Department/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DepartmentCreateModel model)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (this.DepartmentLogic.GetByCode(model.Code) != null)
                    {
                        throw new FailureException("Code already exists. Please use another code");
                    }
                    DepartmentLogic.Save(new Store.Core.Department
                    {
                        Code = model.Code,
                        Description = model.Description,
                        Name = model.Name,
                        DateCreated = DateTime.Now

                    });
                    this.InventoryAnalysis.TotalDepartment++;
                    ProductAnalysisLogic.Update(this.InventoryAnalysis);
                    DepartmentLogic.SaveChanges();
                    ViewBag.Success = $"{model.Name} Successfully Saved";
                    ModelState.Clear();
                    return View(new DepartmentCreateModel());
                }
               
            }
            catch (FailureException ex)
            {
                ViewBag.Error = ex.Message;

            }
            catch (Exception ex)
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";
                return View();
            }
            return View();
        }
        public ActionResult ViewDepartments(DepartmentViewModel model)
        {
            try
            {
                var Department = DepartmentLogic.GetAll();

                DepartmentViewModel theModel = new DepartmentViewModel { TheDepartments = Department };

                return View(theModel);
            }
            catch
            {
                ViewBag.Error = "System Error Occured. Please try again shortly";
                return View();
            }
        }
        // GET: Department/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Department/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Department/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}