﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyFAANStore.Models;
using Store.Core;
using Store.Logic.Interfaces;

namespace MyFAANStore.Controllers
{
    public class HomeController : Controller

    {
        IProductHistoryLogic ProductHistoryLogic;
        IDepartmentLogic DepartmentLogic;
        IProductLogic ProductLogic;
        IProductAnalysisLogic ProductAnalysisLogic;
        public HomeController(IProductHistoryLogic ProductHistoryLogic, IDepartmentLogic departmentLogic, IProductLogic productLogic, IProductAnalysisLogic productAnalysisLogic)
        {
            this.ProductHistoryLogic = ProductHistoryLogic;
            this.ProductLogic = productLogic;
            this.DepartmentLogic = departmentLogic;
            this.ProductAnalysisLogic = productAnalysisLogic;
        }
        public IActionResult Index()
        {
           
            var today = this.ProductAnalysisLogic.GetToday();
            var depart = DepartmentLogic.GetAll() ?? new List<Department>();
            if (today == null)
            {

                today = new Store.Core.ProductAnalysis
                {
                    Date = DateTime.Now,
                    TotalDepartment = DepartmentLogic.GetAll().Count(),
                    TotalIssued = ProductHistoryLogic.Count(Store.Core.Enums.RequestType.Issue),
                    TotalReceived = ProductHistoryLogic.Count(Store.Core.Enums.RequestType.Receive),
                    TotalItems = ProductLogic.Count()
                    
                };
                ProductAnalysisLogic.Save(today);
                ProductAnalysisLogic.SaveChanges();
            }
            var model = new ProductAnalysisModel
            {
                Day = DateTime.Now.Day,
                Month = DateTime.Now.Month,
                Year = DateTime.Now.Year,
                TotalItems=today.TotalItems.ToString(),
                TotalDepartments=today.TotalDepartment.ToString(),
                TotalIssued=today.TotalIssued.ToString(),
                TotalReceived=today.TotalReceived.ToString(),
                Departments=depart
                
            };
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
