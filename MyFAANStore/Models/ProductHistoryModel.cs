﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using Store.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyFAANStore.Models
{


    public class ProductHistoryModel
    {

        [Display(Name = "Product")]
        [Required(ErrorMessage = "*")]
        public long? ProductID { get; set; }
        public IEnumerable<SelectListItem> Products { get; set; }


        public long? DepartmentID { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }

        [Display(Name = "Department / Company / Organisation")]
        [Required(ErrorMessage = "*")]
        public string Department { get; set; }

        [Required(ErrorMessage = "*")]
        public int? Quantity { get; set; }


        [Display(Name = "Name of Representative")]
        public string NameOfRepresentative { get; set; }

        [Display(Name = "Phone No. of Representative")]
        public string PhoneOfRepresentative { get; set; }

        [Display(Name = "Voucher No")]
        public string VoucherNo { get; set; }

        [Display(Name = "Unit Price")]
        public decimal? UnitPrice { get; set; }

        [Display(Name = "Value Price")]
        public decimal ValuePrice { get; set; }

        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        [Display(Name = "Quantity Remaining")]
        public int QuantityRemaining { get; set; }
        public long ID { get; set; }


    }
    public class ProductHistoryViewModel
    {

        public int? Page { get; set; }
        public IList<ProductHistory> TheProductHistories { get; set; }

        [Display(Name = "Search Parameter")]

        public string SearchParameter { get; set; }
    }

    public class ProductHistoryDetailsViewModel
    {
        [Display(Name = "Voucher No.")]
        public string ProductCode { get; set; }

        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Display(Name = "Department")]
        public string Department { get; set; }

        [Display(Name = "Request Type")]
        public RequestType RequestType { get; set; }

        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        [Display(Name = "Qty.")]
        public int Quantity { get; set; }

        [Display(Name = "Qty. After")]
        public int QuantityAfter { get; set; }

        [Display(Name = "Qty. Before")]
        public int QuantityBefore { get; set; }

        [Display(Name = "Name of Rep.")]
        public string NameOfRepresentative { get; set; }

        [Display(Name = "Phone of Rep.")]
        public string PhoneOfRepresentative { get; set; }

        [Display(Name = "Value Price")]
        public decimal ValuePrice { get; set; }

        [Display(Name = "Unit Price")]
        public decimal UnitPrice { get; set; }


    }

    public class ProductVerifyModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "*")]
        [Display(Name = "Voucher No.")]
        public string ProductCode { get; set; }
        public bool IsSupply { get; set; }
        public bool IsInitialRequest { get; set; }
    }

    public class ProductHistoryExportModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        [EnumDataType(typeof(RequestType))]
        [Display(Name = "Request Type")]
        public RequestType RequestType { get; set; }
        public long? DepartmentID { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }

        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }
    }

}
