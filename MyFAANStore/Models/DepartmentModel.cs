﻿using Store.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyFAANStore.Models
{
    public class DepartmentCreateModel
    {
        //[Required(ErrorMessage = "Required")]
        //[Display(Name = "Contact Phone")]
        //[RegularExpression(@"^[0-9]{11,13}$", ErrorMessage = "Invalid Phone Number")]

        [Required(AllowEmptyStrings = false, ErrorMessage = "*")]
        public string Code { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "*")]
        [Display(Prompt = "Name")]
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        
    }

    public class DepartmentViewModel
    {
       
            public int? Page { get; set; }
            public IList<Department> TheDepartments { get; set; }

            [Display(Name = "Search Parameter")]

            public string SearchParameter { get; set; }
    }
}
