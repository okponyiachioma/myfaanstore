﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Store.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyFAANStore.Models
{
    public class ProductCreateModel
    {
        //[Required(ErrorMessage = "Required")]
        //[Display(Name = "Contact Phone")]
        //[RegularExpression(@"^[0-9]{11,13}$", ErrorMessage = "Invalid Phone Number")]

        [Required(AllowEmptyStrings = false,ErrorMessage = "*")]
        public string Code { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "*")]
        [Display(Prompt = "Name")]
        public string Name { get; set; }

       
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Total Quantity")]
        public int? Quantity { get; set; }

        [Required(ErrorMessage = "*")]
        public decimal? UnitPrice { get; set; }

        [Display(Name = "Department")]
        [Required(ErrorMessage = "*")]
        public long? DepartmentID { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }
        public long ID { get; set; }
    }

    public class ProductViewModel
    {
       
            public int? Page { get; set; }
            public IList<Product> TheProducts { get; set; }

            [Display(Name = "Search Parameter")]

            public string SearchParameter { get; set; }
    }
    public class ProductDetailsModel
    {
        [Display(Name = "Voucher No.")]
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public string  Department { get; set; }
        public long ID { get; set; }
    }
    public class ProductExportModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    
        public long? DepartmentID { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }
    }
}
