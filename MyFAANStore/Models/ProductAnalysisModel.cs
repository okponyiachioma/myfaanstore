﻿using Store.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFAANStore.Models
{
    public class ProductAnalysisModel
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string TotalReceived { get; set; }
        public string TotalIssued { get; set; }
        public string TotalItems { get; set; }
        public string TotalDepartments { get; set; }

        public IList<Department> Departments { get; set; }

    }
}
