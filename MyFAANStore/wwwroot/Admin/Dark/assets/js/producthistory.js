//These codes takes from http://t4t5.github.io/sweetalert/

$(function() {
    "use strict";

    $('.anchorDetail').on('click', function () {

        

        var $buttonClicked = $(this);
        var TeamDetailPostBackURL = $buttonClicked. attr("href");
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: TeamDetailPostBackURL,
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
                $('#myModalContent').html(data);
                $('#exampleModalLive2').modal(options);
                $('#exampleModalLive2').modal('show');

            },
            error: function () {
                alert("content load failed.");
            }
        });
    });

    $('.editDetail').on('click', function () {



        var $buttonClicked = $(this);
        var TeamDetailPostBackURL = $buttonClicked.attr("href");
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: TeamDetailPostBackURL,
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
              
               

            },
            error: function () {
                alert("content load failed.");
            }
        });
    });
});