﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Store.Data;
using Store.Data.EF;
using Store.Logic.Implementation;
using Store.Logic.Interfaces;

namespace MyFAANStore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc(opt =>
            {
                opt.AllowEmptyInputInBodyModelBinding = true;
                opt.AllowValidatingTopLevelNodes = true;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

          

            services.AddDbContext<StoreDBContext>(item => item.UseSqlServer(Configuration.GetConnectionString("myconn")));
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<IProductHistoryRepository, ProductHistoryRepository>();
            services.AddTransient<IProductAnalysisRepository, ProductAnalysisRepository>();

            services.AddTransient<IProductLogic, ProductLogic>();
            services.AddTransient<IDepartmentLogic, DepartmentLogic>();
            services.AddTransient<IProductHistoryLogic, ProductHistoryLogic>();
            services.AddTransient<IProductAnalysisLogic, ProductAnalysisLogic>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
